# Olá Mundo

Projeto Angular criado para reproduzir aprendizado do curso "*Olá Mundo! com Angular*" do **DevMedia**.

Neste projeto foi utilizado de conhecimentos básicos de Angular utilizando o assistente via console [Angular CLI](https://github.com/angular/angular-cli) e usando dos conceitos de orientação objeto e componentização do framework.

## Recursos utilizados

Esse projeto foi gerado utilizando [Angular CLI](https://github.com/angular/angular-cli) versão 8.0.3.

## Comandos disponíveis

- Execute `ng serve` para iniciar servidor de desenvolvimento.
- Execute `ng generate component nome-do-componente` para gerar um novo componente.
- Execute `ng build` para realizar o build do projeto.
- Execute `ng test` para executar os testes unitários utilizando [Karma](https://karma-runner.github.io).
- Execute `ng e2e` para executar os testes end-to-end utilizando [Protractor](http://www.protractortest.org/).